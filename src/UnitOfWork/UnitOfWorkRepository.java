package UnitOfWork;

import TableModels.Entity;

public interface UnitOfWorkRepository {

	void persistAdd(Entity entity);
	void persistDelete(Entity entity);
	void persistUpdate(Entity entity);
}
