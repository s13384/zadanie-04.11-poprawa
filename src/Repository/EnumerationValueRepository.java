package Repository;

import java.util.ArrayList;

import TableModels.EnumerationValue;;

public interface EnumerationValueRepository extends Repository<EnumerationValue>{

	ArrayList<EnumerationValue> withName(String name);
	ArrayList<EnumerationValue> withIntKey(int key, String name);
	ArrayList<EnumerationValue> withStringKey(String key, String name);
}
