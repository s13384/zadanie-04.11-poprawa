package SQLites;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Repository.EnumerationValueRepository;
import Repository.PagingInfo;
import TableModels.Entity;
import TableModels.EntityState;
import TableModels.EnumerationValue;
import TableModels.User;
import UnitOfWork.UnitOfWorkRepository;
import UnitOfWork.UnitOfWork;

public class SQLiteEnumerationValuesRepository implements EnumerationValueRepository, UnitOfWorkRepository{

	Connection conn;
	
	PreparedStatement selectCount;
	PreparedStatement selectAll;
	PreparedStatement selectWithId;
	PreparedStatement selectWithEnumName;
	PreparedStatement selectWithIntKeyAndEnumName;
	PreparedStatement selectWithStringKeyAndEnumName;
	PreparedStatement insertIntKeyAndStringKeyAndValueAndEnumName;
	PreparedStatement deleteWithId;
	PreparedStatement updateIntKeyAndStringKeyAndValueAndEnumNameWithId;
	
	SQLiteEnumerationValuesRepository() {
		try {
	       Class.forName("org.sqlite.JDBC");
	       conn = DriverManager.getConnection("jdbc:sqlite:test.db");         
	       conn.setAutoCommit(false);
	       
	       selectCount = conn.prepareStatement("SELECT count(*) from t_sys_enums");
	       selectAll = conn.prepareStatement("SELECT intKey, stringKey, value, enumeration_name FROM t_sys_enumerations ");
	       selectWithId = conn.prepareStatement("SELECT intKey, stringKey, value, enumeration_name FROM t_sys_enumerations WHERE id = ?");
	       selectWithEnumName = conn.prepareStatement("SELECT intKey, stringKey, value, enumeration_name FROM t_sys_enumerations WHERE enumeration_name = ?");
	       selectWithIntKeyAndEnumName = conn.prepareStatement("SELECT intKey, stringKey, value, enumeration_name FROM t_sys_enumerations WHERE intKey = ? and enumeration_name = ?");
	       selectWithStringKeyAndEnumName = conn.prepareStatement("SELECT intKey, stringKey, value, enumeration_name FROM t_sys_enumerations WHERE stringKey = ? and enumeration_name = ?");
	       insertIntKeyAndStringKeyAndValueAndEnumName = conn.prepareStatement("INSERT INTO t_sys_enums(intKey, stringKey, value, enumerationName) VALUES(?, ?, ?,?)");
	       deleteWithId = conn.prepareStatement("DELETE FROM t_sys_enums WHERE id = ?");
	       updateIntKeyAndStringKeyAndValueAndEnumNameWithId = conn.prepareStatement("UPDATE t_sys_enums set intKey = ?, stringKey = ?, value = ?, enumerationName = ? WHERE id = ?");
	            
			}catch (SQLException ex) {
	        	ex.printStackTrace();
	        }  catch (Exception ex) {
	        	ex.printStackTrace();
	        }
	}
	@Override
	public EnumerationValue withId(int id) {
		EnumerationValue enumer=null;
        try {
            selectWithId.setInt(1, id);
            ResultSet rows = selectWithId.executeQuery();
            
            while(rows.next()){
            	enumer = new EnumerationValue(
            			rows.getInt("intKey"),
            			rows.getString("stringKey"),
            			rows.getString("value"),
            			rows.getString("enumeration_name")   );
            }
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
        return enumer;
	}

	@Override
	public ArrayList<EnumerationValue> allOnPage(PagingInfo page) {
		ArrayList<EnumerationValue> enumers=new ArrayList<EnumerationValue>();
        try {
            ResultSet rows = selectAll.executeQuery();
            
            while(rows.next()){
            	enumers.add( new EnumerationValue(
            			rows.getInt("intKey"),
            			rows.getString("stringKey"),
            			rows.getString("value"),
            			rows.getString("enumeration_name")   ));
            }
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
        return enumers;
	}

	@Override
	public void add(EnumerationValue entity) {
	      try {
		insertIntKeyAndStringKeyAndValueAndEnumName.setInt(1, entity.getIntKey());
		insertIntKeyAndStringKeyAndValueAndEnumName.setString(2, entity.getStringKey());
		insertIntKeyAndStringKeyAndValueAndEnumName.setString(3, entity.getValue());
		insertIntKeyAndStringKeyAndValueAndEnumName.setString(4, entity.getEnumerationName());
		insertIntKeyAndStringKeyAndValueAndEnumName.executeUpdate();
	      }catch (SQLException e){
	    	  e.printStackTrace();
	      }
	}
	@Override
	public void delete(EnumerationValue entity) {
		try {
		deleteWithId.setInt(1, entity.getId());	
		deleteWithId.executeUpdate();
		} catch ( SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public void modify(EnumerationValue entity) {
		try {
		updateIntKeyAndStringKeyAndValueAndEnumNameWithId.setInt(1, entity.getIntKey());
		updateIntKeyAndStringKeyAndValueAndEnumNameWithId.setString(2, entity.getStringKey());
		updateIntKeyAndStringKeyAndValueAndEnumNameWithId.setString(3, entity.getValue());
		updateIntKeyAndStringKeyAndValueAndEnumNameWithId.setString(4, entity.getEnumerationName());
		updateIntKeyAndStringKeyAndValueAndEnumNameWithId.setInt(5, entity.getId());
		updateIntKeyAndStringKeyAndValueAndEnumNameWithId.executeUpdate();
		} catch (SQLException e){
			e.printStackTrace();
		}
	}

	@Override
	public int count() {
		int tmp=0;
		try {
            ResultSet rows = selectCount.executeQuery();
            
            while(rows.next()){
            	tmp = rows.getInt("count");
            }
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
		return tmp;
	}

	@Override
	public ArrayList<EnumerationValue> withName(String name) {
		ArrayList<EnumerationValue> enumers=new ArrayList<EnumerationValue>();
        try {
            selectWithEnumName.setString(1, name);
            ResultSet rows = selectWithEnumName.executeQuery();
            
            while(rows.next()){
            	enumers.add( new EnumerationValue(
            			rows.getInt("intKey"),
            			rows.getString("stringKey"),
            			rows.getString("value"),
            			rows.getString("enumeration_name")   ));
            }
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
        return enumers;
	}

	@Override
	public ArrayList<EnumerationValue> withIntKey(int key, String name) {
		ArrayList<EnumerationValue> enumers=new ArrayList<EnumerationValue>();
        try {
        	selectWithIntKeyAndEnumName.setInt(1, key);
        	selectWithIntKeyAndEnumName.setString(2, name);
            ResultSet rows = selectWithIntKeyAndEnumName.executeQuery();
            
            while(rows.next()){
            	enumers.add( new EnumerationValue(
            			rows.getInt("intKey"),
            			rows.getString("stringKey"),
            			rows.getString("value"),
            			rows.getString("enumeration_name")   ));
            }
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
        return enumers;
	}

	@Override
	public ArrayList<EnumerationValue> withStringKey(String key, String name) {
		ArrayList<EnumerationValue> enumers=new ArrayList<EnumerationValue>();
        try {
            selectWithStringKeyAndEnumName.setString(1, key);
            selectWithStringKeyAndEnumName.setString(2, name);
            ResultSet rows = selectWithStringKeyAndEnumName.executeQuery();
            
            while(rows.next()){
            	enumers.add( new EnumerationValue(
            			rows.getInt("intKey"),
            			rows.getString("stringKey"),
            			rows.getString("value"),
            			rows.getString("enumeration_name")   ));
            }
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
        return enumers;
	}

	@Override
	public void persistAdd(Entity entity) {
		try {
			setUpInsertQuery((EnumerationValue)entity);
			insertIntKeyAndStringKeyAndValueAndEnumName.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void persistDelete(Entity entity) {
		try {
			deleteWithId.setInt(1, entity.getId());
			deleteWithId.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void persistUpdate(Entity entity) {
		try {
			setUpUpdateQuery((EnumerationValue)entity);
			updateIntKeyAndStringKeyAndValueAndEnumNameWithId.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	private void setUpInsertQuery(EnumerationValue entity) throws SQLException {
		
		insertIntKeyAndStringKeyAndValueAndEnumName.setInt(1, entity.getIntKey());
		insertIntKeyAndStringKeyAndValueAndEnumName.setString(2, entity.getStringKey());
		insertIntKeyAndStringKeyAndValueAndEnumName.setString(3, entity.getValue());
		insertIntKeyAndStringKeyAndValueAndEnumName.setString(4, entity.getEnumerationName());
		
	}

	private void setUpUpdateQuery(EnumerationValue entity) throws SQLException {
		
		updateIntKeyAndStringKeyAndValueAndEnumNameWithId.setInt(1, entity.getIntKey());
		updateIntKeyAndStringKeyAndValueAndEnumNameWithId.setString(2, entity.getStringKey());
		updateIntKeyAndStringKeyAndValueAndEnumNameWithId.setString(3, entity.getValue());
		updateIntKeyAndStringKeyAndValueAndEnumNameWithId.setString(4, entity.getEnumerationName());
		updateIntKeyAndStringKeyAndValueAndEnumNameWithId.setInt(5, entity.getId());
		
		
	}

}