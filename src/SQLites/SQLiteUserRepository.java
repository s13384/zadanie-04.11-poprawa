package SQLites;

import java.util.ArrayList;

import Repository.*;
import TableModels.*;
import UnitOfWork.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLiteUserRepository implements UserRepository, UnitOfWorkRepository{

	Connection conn;
	PreparedStatement updateSetLoginAndPasswordWithId;
	PreparedStatement selectCount;
	PreparedStatement selectNoParam;
	PreparedStatement selectWithId;
	PreparedStatement selectWithLogin;
	PreparedStatement selectWithLoginAndPassword;
	PreparedStatement insertIdAndLoginAndPassword;
	PreparedStatement deleteWithId;
	PreparedStatement updateRoleIdWithId;
	
	public SQLiteUserRepository() {
		begin();
	}
	public void begin(){
		try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:test.db");         
            conn.setAutoCommit(false);
            updateSetLoginAndPasswordWithId = conn.prepareStatement("UPDATE t_sys_users set login = ? , password = ? WHERE id = ?");
            selectWithId = conn.prepareStatement("SELECT id, login, password FROM t_sys_users where id = ?");
            selectNoParam = conn.prepareStatement("SELECT id, login, password FROM t_sys_users");
            insertIdAndLoginAndPassword = conn.prepareStatement("INSERT INTO t_sys_users(id, login, password) VALUES(?, ?, ?)");
            deleteWithId = conn.prepareStatement("DELETE FROM t_sys_users WHERE id = ? ");
            selectCount = conn.prepareStatement("SELECT count(*) from t_sys_users");
            selectWithLogin = conn.prepareStatement("SELECT id, login, password FROM t_sys_users WHERE login = ?");
            selectWithLoginAndPassword = conn.prepareStatement("SELECT id, login, password FROM t_sys_users WHERE login = ? and password = ?");
            updateRoleIdWithId = conn.prepareStatement("UPDATE t_sys_roles SET roleId = ? WHERE userId = ?");
            
		}catch (SQLException ex) {
        	ex.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
	}
	public void exit(){
		try {
			if (!conn.isClosed())
			conn.close();
			
		} catch (SQLException e) {		
			e.printStackTrace();
		}catch (Exception ex) {
        	ex.printStackTrace();
        }
	}
	@Override
	public User withId(int id) {	
		User user = null;
        try {
            selectWithId.setInt(1, id);
            ResultSet rows = selectWithId.executeQuery();
            
            while(rows.next()){
            	user = new User(
            			rows.getInt("id"),
            			rows.getString("login"),
            			rows.getString("password")   );
            }
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
        return user;	
	}

	@Override
	public ArrayList<User> allOnPage(PagingInfo page) {
		ArrayList<User> users = new ArrayList<User>();
		
        try {
            ResultSet rows = selectNoParam.executeQuery();
            
            while(rows.next()){
            	users.add(new User(
            			rows.getInt("id"),
            			rows.getString("login"),
            			rows.getString("password")   ));
            }
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
        return users;	
	}

	@Override
	public void add(User entity) {
		try {
			insertIdAndLoginAndPassword.setInt(1,entity.getId());
			insertIdAndLoginAndPassword.setString(2,entity.getLogin());
			insertIdAndLoginAndPassword.setString(3, entity.getPassword());
			insertIdAndLoginAndPassword.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void delete(User entity) {
		try {
			deleteWithId.setInt(1, entity.getId());
			deleteWithId.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void modify(User entity) {	
		try {
		updateSetLoginAndPasswordWithId.setString(1, entity.getLogin());
		updateSetLoginAndPasswordWithId.setString(2, entity.getPassword());
		updateSetLoginAndPasswordWithId.setInt(3, entity.getId());
		updateSetLoginAndPasswordWithId.executeUpdate();
		} catch (SQLException se){
			se.printStackTrace();
		}
		
	}

	@Override
	public int count() {
		int tmp=0;
		try {
            ResultSet rows = selectCount.executeQuery();
            
            while(rows.next()){
            	tmp = rows.getInt("count");
            }
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
		return tmp;
	}

	@Override
	public ArrayList<User> withLogin(String login) {
		ArrayList<User> users = new ArrayList<User>();
		
        try {
            selectWithLogin.setString(1, login);
            ResultSet rows = selectWithLogin.executeQuery();
            
            while(rows.next()){
            	users.add(new User(
            			rows.getInt("id"),
            			rows.getString("login"),
            			rows.getString("password")   ));
            }
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
        return users;	
	}

	@Override
	public ArrayList<User> withLoginAndPassword(String login, String password) {
		ArrayList<User> users = new ArrayList<User>();
		
        try {
            selectWithLoginAndPassword.setString(1, login);
            selectWithLoginAndPassword.setString(2, password);
            ResultSet rows = selectWithLoginAndPassword.executeQuery();
            
            while(rows.next()){
            	users.add(new User(
            			rows.getInt("id"),
            			rows.getString("login"),
            			rows.getString("password")   ));
            }
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
        return users;
	}

	@Override
	public void setupPermissions(User user) {
        try {
            updateRoleIdWithId.setInt(1, 1);
            updateRoleIdWithId.setInt(2, user.getId());
            updateRoleIdWithId.executeUpdate();
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
	}

	@Override
	public void persistAdd(Entity entity) {
		try {
			setUpInsertQuery((User)entity);
			insertIdAndLoginAndPassword.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void persistDelete(Entity entity) {
		try {
			deleteWithId.setInt(1, entity.getId());
			deleteWithId.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void persistUpdate(Entity entity) {
		try {
			setUpUpdateQuery((User)entity);
			updateSetLoginAndPasswordWithId.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	private void setUpInsertQuery(User entity) throws SQLException {
		
		insertIdAndLoginAndPassword.setInt(1, entity.getId());
		insertIdAndLoginAndPassword.setString(2, entity.getLogin());
		insertIdAndLoginAndPassword.setString(2, entity.getPassword());
		
	}

	private void setUpUpdateQuery(User entity) throws SQLException {
		
		updateSetLoginAndPasswordWithId.setString(1, entity.getLogin());
		updateSetLoginAndPasswordWithId.setString(2, entity.getPassword());
		updateSetLoginAndPasswordWithId.setInt(3, entity.getId());
		
		
	}

}
	
