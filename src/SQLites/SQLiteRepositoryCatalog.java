package SQLites;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Repository.EnumerationValueRepository;
import Repository.RepositoryCatalog;
import Repository.UserRepository;
import TableModels.EnumerationValue;
import TableModels.User;

public class SQLiteRepositoryCatalog implements RepositoryCatalog{

	Connection conn;
	
	PreparedStatement selectFromUsers;
	PreparedStatement selectFromEnums;
	
	public SQLiteRepositoryCatalog() {
		begin();
	}
	public void begin(){
		try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:test.db");
            selectFromUsers = conn.prepareStatement("SELECT id, login, password FROM t_sys_users");
            selectFromEnums = conn.prepareStatement("SELECT intKey, stringKey, value, enumeration_name FROM t_sys_enumerations");
		}catch (SQLException ex) {
        	ex.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
	}

	@Override
	public EnumerationValueRepository enumerations() {
		EnumerationValueRepository repo = new SQLiteEnumerationValuesRepository();
        try {
            ResultSet rows = selectFromEnums.executeQuery();
            
            while(rows.next()){
            	repo.add(new EnumerationValue(
            			rows.getInt("intKey"),
            			rows.getString("stringKey"),
            			rows.getString("value"),
            			rows.getString("enumeration_name")   ));
            }
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
        return repo;
	}

	@Override
	public UserRepository users() {
			UserRepository repo = new SQLiteUserRepository();
		
        try {
            ResultSet rows = selectFromUsers.executeQuery();
            
            while(rows.next()){
            	repo.add(new User(
            			rows.getInt("id"),
            			rows.getString("login"),
            			rows.getString("password")   ));
            }
        } catch (SQLException e) {		
			e.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
        return repo;
	}

}
