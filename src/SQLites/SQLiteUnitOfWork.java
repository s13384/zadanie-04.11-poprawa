package SQLites;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import TableModels.*;
import UnitOfWork.*;

public class SQLiteUnitOfWork implements UnitOfWork{

	private Map<Entity, UnitOfWorkRepository> entities = new HashMap<Entity, UnitOfWorkRepository>();
	
	Connection conn;
	
	public SQLiteUnitOfWork() {
		begin();
	}
	public void begin(){
		try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:test.db");         
            conn.setAutoCommit(false);
            
		}catch (SQLException ex) {
        	ex.printStackTrace();
        }  catch (Exception ex) {
        	ex.printStackTrace();
        }
	}
	public void exit(){
		try {
			if (!conn.isClosed())
			conn.close();
			
		} catch (SQLException e) {		
			e.printStackTrace();
		}catch (Exception ex) {
        	ex.printStackTrace();
        }
	}
	@Override
	public void saveChanges() {
		try {
			if (!conn.isClosed())
				
				for(Entity entity: entities.keySet())
				{
					switch(entity.getEntityState())
					{
						case Modified:
							entities.get(entity).persistUpdate(entity);
							break;
						case Deleted:
							entities.get(entity).persistDelete(entity);
							break;
						case New:
							entities.get(entity).persistAdd(entity);
							break;
						case UnChanged:
							break;
						default:
							break;
					}
				}
            conn.commit();       
            
		}catch (SQLException ex) {
        	ex.printStackTrace();
        }catch (Exception ex) {
        	ex.printStackTrace();
        }	
	}

	@Override
	public void undo() {
		try {
			if (!conn.isClosed())
            conn.rollback();      
            
		}catch (SQLException ex) {
        	ex.printStackTrace();
        }catch (Exception ex) {
        	ex.printStackTrace();
        }
	}

	@Override
	public void markAsNew(Entity entity, UnitOfWorkRepository repo) {
		entity.setEntityState(EntityState.New);
		entities.put(entity, repo);
	}

	@Override
	public void markAsDeleted(Entity entity, UnitOfWorkRepository repo) {
		entity.setEntityState(EntityState.Deleted);
		entities.put(entity, repo);
	}

	@Override
	public void markAsChanged(Entity entity, UnitOfWorkRepository repo) {
		entity.setEntityState(EntityState.Modified);
		entities.put(entity, repo);	
	}

}
