package TableModels;

public abstract class Entity {

	protected int id;
	protected EntityState state;
	
	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}
	public EntityState getEntityState(){
		return state;
	}
	public void setEntityState(EntityState state){
		this.state = state;
	}
}
